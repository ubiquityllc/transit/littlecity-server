package transit.littlecity.impl.services

import gigahorse.{FullResponse, Request}
import io.circe.Json
import io.circe.parser.parse
import transit.littlecity.api.ApiClient
import transit.littlecity.data.message.{Agency, RouteArrivalDeparture}
import transit.littlecity.data.model.BusStop
import transit.littlecity.impl.Implicits._
import transit.littlecity.services.OneBusAwayService

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

/**
 * The implementation details for the [[OneBusAwayService]] trait
 */
trait OneBusAwayServiceComponent {

   import OneBusAwayServiceComponent._

   /**
    * Onebusaway api wrapper
    */
   class OneBusAway(config: OneBusAwayConfig) extends OneBusAwayService with AutoCloseable {

      private val agencyId = config.agency
      private val apiEndPoint = config.obaUrl
      private val httpClient = ApiClient(apiEndPoint)
      private val baseRequest = Request("").withQueryString("key" -> "TEST")

      override def agencyInfo: Future[Agency] =
         httpClient.run(request(s"/where/agency/$agencyId.json").get,
            jsonTransform { json =>
               json.hcursor.downField("data").downField("entry").as[Agency].toOption.get
            })

      override def findAvailableStops: Future[List[BusStop]] = for {
         stopIds <- findStopIds
         stopInfo <- Future.traverse(stopIds)(findStopInfo)
      } yield stopInfo

      /**
       * Finds all the stops for this `agencyId`
       */
      private def findStopIds: Future[List[String]] =
         httpClient.run(request(s"/where/stop-ids-for-agency/$agencyId.json").get
            .addQueryString("includeReferences" -> "false"), jsonTransform { json =>

            json.hcursor.downField("data").get[List[String]]("list").getOrElse(List())
         })

      /**
       * Finds stop information for the given `stopId`
       *
       * @param stopId The id of the stop
       * @return The info and routes associated with this stop
       */
      override def findStopInfo(stopId: String): Future[BusStop] =
         httpClient.run(request(s"/where/stop/$stopId.json").get, jsonTransform { json =>
            json.hcursor.get[BusStop]("data").toOption.get
         })

      override def arrivalsAndDeparturesForStop(stopId: String): Future[List[RouteArrivalDeparture]] =
         httpClient.run(request(s"/where/arrivals-and-departures-for-stop/$stopId.json").get
            .addQueryString("includeReferences" -> "false")
            .addQueryString("minutesAfter" -> "60"), // TODO: Compute appropriate offset for certain times
            jsonTransform { json =>
               val arrivalDeparture = json.hcursor.downField("data").downField("entry").downField("arrivalsAndDepartures")
               arrivalDeparture.as[List[RouteArrivalDeparture]].getOrElse(List())
            }
         )

      private def request(apiPath: String): Request = baseRequest.withUrl(apiPath)

      private def jsonTransform[T](transform: Json => T)(response: FullResponse): T =
         transform(parse(response.bodyAsString).getOrElse(Json.Null))

      override def close(): Unit = httpClient.close()
   }

}

object OneBusAwayServiceComponent {
   case class OneBusAwayConfig(obaUrl: String, agency: String)
}
