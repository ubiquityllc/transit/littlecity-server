package transit.littlecity.impl.services

import org.zeromq.ZMQ
import org.zeromq.ZMQ.Socket
import org.zeromq.core.ZAuth
import transit.littlecity.zmq.zmqAuth
import transit.littlecity.services.AuthenticationService

import scala.util.chaining._

trait AuthenticationServiceComponent {

   class PlainAuthentication extends AuthenticationService {
      val auth: ZAuth = zmqAuth.tap { auth =>
         auth.setVerbose(true)
         auth.configurePlain(ZAuth.CURVE_ALLOW_ANY,
            ClassLoader.getSystemResource("passwords.txt").getFile)
      }

      override def configureServer(socket: Socket): Unit = {
         socket.setPlainServer(true)
         socket.setZAPDomain("global".getBytes(ZMQ.CHARSET))
      }
   }

   class CurveAuthentication extends AuthenticationService {
      val auth: ZAuth = zmqAuth.tap { auth =>
         auth.setVerbose(true)
         auth.configureCurve(ZAuth.CURVE_ALLOW_ANY)
      }

      override def configureServer(socket: Socket): Unit = {
         socket.setCurveServer(true)
         socket.setZAPDomain("global".getBytes(ZMQ.CHARSET))
      }
   }
}
