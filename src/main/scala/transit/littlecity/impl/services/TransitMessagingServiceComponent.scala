package transit.littlecity.impl.services

import java.time.Instant

import monix.eval.Task
import monix.execution.Scheduler.Implicits.global
import monix.execution.cancelables.CompositeCancelable
import monix.execution.{Cancelable, Scheduler}
import monix.reactive.subjects.ConcurrentSubject
import monix.reactive.{Consumer, Observable}
import org.zeromq.ZMQ
import transit.littlecity.data.message.{Agency, RouteArrivalDeparture, StopRoutesMessage}
import transit.littlecity.messaging
import transit.littlecity.messaging.StopInformation
import transit.littlecity.services.{AuthenticationService, OneBusAwayService, TransitMessagingService}
import transit.littlecity.zmq._
import wvlet.log.LogSupport

import scala.concurrent.Future
import scala.concurrent.duration._
import scala.util.chaining._

trait TransitMessagingServiceComponent {

   import TransitMessagingServiceComponent._
   import transit.littlecity.impl.Implicits._

   class TransitBroker(config: TransitMessagingConfig,
                       oneBusAwayService: OneBusAwayService,
                       authService: AuthenticationService) extends TransitMessagingService with AutoCloseable with LogSupport {

      private val registerSocket = pull.tap { sock =>
         authService.configureServer(sock)
         sock.bind(config.registerEndpoint)
         sock.setLinger(0)
      }

      private val serverSocket = publisher.tap { sock =>
         authService.configureServer(sock)
         sock.bind(config.publishEndpoint)
      }

      private val serverMonitor = serverSocket.monitor { evt: MonitorEvent =>
         info(s"Got new event (${evt.event}) @ address: ${evt.address}")
      }

      private val nodes = scala.collection.mutable.Map[String, Cancelable]()
      private val routesUpdatePublisher = ConcurrentSubject.publish[messaging.StopRoutesMessage]
      private val registerPublisher = ConcurrentSubject.publishToOne[String]
      private val socketFutures = CompositeCancelable()

      override def registerBusStopNode(stopId: String): Unit = {
         info(s"Registering stop: $stopId...")
         registerPublisher.onNext(stopId)
      }

      override def unregisterBusStopNode(stopId: String): Unit = {
         nodes.remove(stopId).foreach {
            info(s"Unregistered stop: $stopId")
            _.cancel()
         }
      }

      override def startPublishing(): Unit = {
         val io = Scheduler.io()
         info("Registering publisher...")
         socketFutures += routesUpdatePublisher
            .observeOn(io)
            .bufferTimed(10.seconds) // TODO: Choose better buffering that plays fair with reactive
            .map(_.groupBy(_.stopId()).transform((_, g) => g.headOption))
            .flatMap(stopUpdates => Observable.fromIterable(stopUpdates.values.flatten))
            .consumeWith(Consumer.foreach { message =>
               info(s"Sending a message to: ${message.stopId()}")
               serverSocket.sendMore(message.stopId())
               serverSocket.send(message.getByteBuffer.array(), 0)
            })
            .runToFuture
         info("Registered publisher")

         info("Starting stop info publisher...")
         socketFutures += registerPublisher
            // Unregister in case of a failed stop
            .doOnNext(s => Task.eval (unregisterBusStopNode(s)))
            .observeOn(io)
            .mapEvalF(stopInformation)
            .consumeWith(Consumer.foreach { stopInfo =>
               val stopId = stopInfo.stopId()
               serverSocket.sendMore(stopId)
               serverSocket.send(stopInfo.getByteBuffer.array(), 0)

               nodes += stopId -> createStopObservable(stopId).subscribe(routesUpdatePublisher)
               info(s"Registered stop: $stopId")
            })
            .runToFuture
         info("Started stop info publisher")

         info("Starting stop ids register...")
         socketFutures += Observable.intervalAtFixedRate(100.millis)
            .observeOn(io)
            .map(_ => registerSocket.recvStr(ZMQ.DONTWAIT, ZMQ.CHARSET))
            .filterNot(s => s == null || s.isEmpty)
            .consumeWith(Consumer.foreach(registerBusStopNode))
            .runToFuture
         info("Started stop ids register")
      }

      override def close(): Unit = {
         info("Closing socket and publisher...")
         stopPublishing()
         registerSocket.close()
         serverSocket.close()
         serverMonitor.close()
         info("Closed socket and publisher")
      }

      override def stopPublishing(): Unit = {
         socketFutures.cancel()
         info("Stopped publisher and register")
      }

      private def createStopObservable(stopId: String): Observable[messaging.StopRoutesMessage] = {
         Observable.intervalAtFixedRate(1.second) // TODO: Select a better interval or dynamic interval
            .mapEvalF(_ => oneBusAwayService.arrivalsAndDeparturesForStop(stopId))
            .switchMap {
               case List() => Observable.eval(List(RouteArrivalDeparture("Saskatoon Transit_10753", "Foobar",
                  Instant.now().minusSeconds(4000), Instant.now().plusSeconds(3000))))
               case l => Observable.eval(l)
            }
            .map(StopRoutesMessage(stopId, _))

      }

      /**
       * Get the stop information for the given stopId
       * @param stopId The stopId we are interested in
       * @return The stop information as a future
       */
      private def stopInformation(stopId: String): Future[StopInformation] = {
         for (busStop <- oneBusAwayService.findStopInfo(stopId);
              agency <- oneBusAwayService.agencyInfo) yield {
            implicit val implAgency: Agency = agency
            busStop
         }
      }
   }

}

object TransitMessagingServiceComponent {

   case class TransitMessagingConfig(publishEndpoint: String, registerEndpoint: String)

}
