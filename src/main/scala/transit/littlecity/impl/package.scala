package transit.littlecity

import java.nio.ByteBuffer
import java.time.{Instant, ZoneId}

import com.google.flatbuffers.FlatBufferBuilder
import io.circe.{Decoder, HCursor}
import transit.littlecity.data.message.{Agency, RouteArrivalDeparture, StopRoutesMessage}
import transit.littlecity.data.model.{BusStop, GeoLocation, Route}
import transit.littlecity.messaging.{RouteInformation, StopInformation, RouteArrivalDeparture => RouteAD, StopRoutesMessage => StopRM}

import scala.concurrent.duration._
import scala.language.{implicitConversions, postfixOps}

package object impl {

   object Implicits {
      implicit def decodeAgency: Decoder[Agency] =
         Decoder.forProduct2("name", "timezone") { (name: String, timezone: String) =>
            Agency(name, ZoneId.of(timezone))
         }

      implicit def decodeGeoLoc: Decoder[GeoLocation] =
         Decoder.forProduct2("lat", "lon") { (lat: Double, lon: Double) =>
            GeoLocation(lat, lon)
         }

      implicit def decodeRoute: Decoder[Route] =
         Decoder.forProduct3("id", "longName", "shortName") { (id: String, longName: String, shortName: String) =>
            Route(id, longName, shortName)
         }

      implicit def decodeBusStop: Decoder[BusStop] = (cursor: HCursor) => {
         val entry = cursor.downField("entry")
         val routeCursor = cursor.downField("references").downField("routes")
         for {
            id <- entry.get[String]("id")
            name <- entry.get[String]("name")
            stopLocationGeo <- entry.as[GeoLocation]
            routes <- routeCursor.as[List[Route]]
         } yield BusStop(id, name, stopLocationGeo, routes)
      }

      implicit def decodeArrivalDeparture: Decoder[RouteArrivalDeparture] = cursor => {
         val realTime = cursor.get[Boolean]("predicted").getOrElse(false)
         val busName = cursor.get[String]("tripHeadsign").getOrElse("")
         for {
            routeId <- cursor.get[String]("routeId")
            nextArrival <- cursor.get[Long](if (realTime) "predictedArrivalTime" else "scheduledArrivalTime")
            lastSeen = nextArrival - 30.minutes.toMillis /* TODO calculate last time a bus for this route was seen */
         } yield RouteArrivalDeparture(routeId, busName, Instant.ofEpochMilli(lastSeen), Instant.ofEpochMilli(nextArrival))
      }

      /**
       * Implicit converter from the regular stop route message to the one we send
       * over the network
       *
       * @note Can we use ByteBuffer.allocateDirect as a factory to [[FlatBufferBuilder]]?
       * @param stopRoutesMessage The internal stop routes message representation
       * @return The external stop routes message
       */
      implicit def internalStopRouteToExternal(stopRoutesMessage: StopRoutesMessage): StopRM = {
         val buffer = new FlatBufferBuilder(1)
         val routes = stopRoutesMessage.routesArrivalsAndDepartures.map { route =>
            RouteAD.createRouteArrivalDeparture(buffer,
               buffer.createString(route.routeId),
               buffer.createString(route.headSign),
               route.lastSeen.toEpochMilli,
               route.nextArrival.toEpochMilli)
         }
         val routesVec = StopRM createRoutesArrivalsAndDeparturesVector(buffer, routes.toArray)
         val stop = StopRM.createStopRoutesMessage(buffer, buffer.createString(stopRoutesMessage.stopId), routesVec)
         buffer.finish(stop)

         StopRM.getRootAsStopRoutesMessage(ByteBuffer.wrap(buffer.sizedByteArray()))
      }

      implicit def busStopToStopToStopInformation(busStop: BusStop)(implicit agency: Agency): StopInformation = {
         val buffer = new FlatBufferBuilder(1)
         val routes = busStop.routes.map { route =>
            RouteInformation.createRouteInformation(buffer,
               buffer.createString(route.id),
               buffer.createString(route.name),
               buffer.createString(route.shortName)
            )
         }
         val routesVec = StopInformation.createRouteInformationListVector(buffer, routes.toArray)
         val stopLoc = StopInformation.createStopInformation(buffer,
            buffer.createString(busStop.id),
            buffer.createString(busStop.stopLocationName),
            buffer.createString(agency.timezone.getId),
            messaging.GeoLocation.createGeoLocation(buffer,
               busStop.stopLocationGeo.longitude.toFloat, busStop.stopLocationGeo.latitude.toFloat),
            routesVec
         )

         buffer.finish(stopLoc)
         StopInformation.getRootAsStopInformation(ByteBuffer.wrap(buffer.sizedByteArray()))
      }
   }

}
