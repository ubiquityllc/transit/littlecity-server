package transit.littlecity.data

import java.time.{Instant, ZoneId}

package object message {

   sealed trait StopMessage {
      val stopId: String
   }

   case class StopRoutesMessage(stopId: String, routesArrivalsAndDepartures: List[RouteArrivalDeparture]) extends StopMessage

   case class RouteArrivalDeparture(routeId: String, headSign: String, lastSeen: Instant, nextArrival: Instant)

   case class Agency(name: String, timezone: ZoneId)

}
