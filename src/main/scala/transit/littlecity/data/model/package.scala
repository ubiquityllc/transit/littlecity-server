package transit.littlecity.data

package object model {

   case class BusStop(id: String, stopLocationName: String, stopLocationGeo: GeoLocation, routes: List[Route])

   case class Route(id: String, name: String, shortName: String)

   case class GeoLocation(latitude: Double, longitude: Double)

}
