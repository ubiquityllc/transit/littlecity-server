package transit.littlecity.config

import transit.littlecity.impl.services.OneBusAwayServiceComponent.OneBusAwayConfig
import transit.littlecity.impl.services.TransitMessagingServiceComponent.TransitMessagingConfig
import transit.littlecity.impl.services.{AuthenticationServiceComponent, OneBusAwayServiceComponent, TransitMessagingServiceComponent}
import transit.littlecity.services.{OneBusAwayService, TransitMessagingService}
import wvlet.airframe.{Design, Session, newDesign}
import wvlet.log.LogFormatter

import scala.util.Properties

trait AppBinding extends OneBusAwayServiceComponent with TransitMessagingServiceComponent with AuthenticationServiceComponent {
   def oneBusAwayConfig: OneBusAwayConfig

   def transitMessagingConfig: TransitMessagingConfig

   def loggingFormatter: LogFormatter

   def localBinding: Design = newDesign
      .bind[LogFormatter].toInstance(loggingFormatter)
      .bind[OneBusAwayService].toInstance(new OneBusAway(oneBusAwayConfig))
      .bind[TransitMessagingService].toProvider(new TransitBroker(transitMessagingConfig, _, _))
}

object AppBinding {
   type Env = String

   def startWithSession[R](body: Session => R): R = {
      di(Properties.envOrElse("", "dev")).withSession { session =>
         val binding = session.build[AppBinding]
         body(session.newSharedChildSession(binding.localBinding))
      }
   }

   private def di(env: Env): Design = env match {
      case _ => newDesign.withLifeCycleLogging.bind[AppBinding].to[DevAppBinding] // For now we just have one environment
   }
}
