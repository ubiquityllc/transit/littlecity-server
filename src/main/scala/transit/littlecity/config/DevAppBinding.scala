package transit.littlecity.config

import transit.littlecity.impl.services.OneBusAwayServiceComponent.OneBusAwayConfig
import transit.littlecity.impl.services.TransitMessagingServiceComponent.TransitMessagingConfig
import transit.littlecity.services.AuthenticationService
import wvlet.airframe.Design
import wvlet.log.LogFormatter
import wvlet.log.LogFormatter.IntelliJLogFormatter

trait DevAppBinding extends AppBinding {
   override val oneBusAwayConfig =
      OneBusAwayConfig("http://localhost:8880/oba-api/api/", "Saskatoon Transit")

   override val transitMessagingConfig =
      TransitMessagingConfig("tcp://*:5667", "tcp://*:5665")

   // https://github.com/wvlet/airframe/tree/master/airframe-log#pre-defined-log-formatters
   override def loggingFormatter: LogFormatter = IntelliJLogFormatter

   override def localBinding: Design = super.localBinding
      .bind[AuthenticationService].to[PlainAuthentication]
}
