package transit.littlecity

import java.util.UUID

import io.lemonlabs.uri.Url
import org.zeromq.{ZMQ, ZMQException}
import org.zeromq.ZMQ.{Event, Socket}
import org.zeromq.core.{ZAuth, ZContext, ZMsg, ZThread}

import scala.jdk.CollectionConverters._
import scala.util.chaining._
import scala.sys.addShutdownHook
import scala.util.control.Breaks
import scala.util.{Failure, Success, Try}

package object zmq {
   implicit val context: ZContext = new ZContext().tap { ctx =>
      ctx.setIoThreads(math.max(Runtime.getRuntime.availableProcessors() >> 2, 1))
      addShutdownHook {
         println("Closing context...")
         ctx.getContext.close()
         println("Closed context!")
      }.setName("ØMQ-Context-ShutdownHook")
   }

   /**
    * Creates a ZMQ_PUB socket
    *
    * @note http://api.zeromq.org/2-1:zmq-socket#toc9
    *       A socket of type ZMQ_PUB is used by a publisher to distribute data.
    *       Messages sent are distributed in a fan out fashion to all connected peers.
    *       The zmq_recv(3) function is not implemented for this socket type.
    *
    *       When a ZMQ_PUB socket enters an exceptional state due to having reached the high water mark
    *       for a subscriber, then any messages that would be sent to the subscriber in question
    *       shall instead be dropped until the exceptional state ends.
    *       The zmq_send() function shall never block for this socket type.
    * @return The socket
    */
   def publisher: Socket = context.createSocket(ZMQ.PUB)
   def dealer: Socket = context.createSocket(ZMQ.DEALER)

   /**
    * Create a ZMQ_PULL socket
    *
    * @note http://api.zeromq.org/2-1:zmq-socket#toc13
    *       A socket of type ZMQ_PULL is used by a pipeline node to receive messages from upstream pipeline nodes.
    *       Messages are fair-queued from among all connected upstream nodes.
    *       The zmq_send() function is not implemented for this socket type.
    * @return
    */
   def pull: Socket = context.createSocket(ZMQ.PULL)

   def zmqAuth: ZAuth = new ZAuth(context)

   implicit class EnhanceSocket(socket: Socket) {
      def send(multipart: String*): Boolean = send(ZMsg.newStringMsg(multipart: _*))

      def send(message: ZMsg): Boolean = {
         message.asScala.reduceOption { (frame1, frame2) =>
            frame1.send(socket, ZMQ.SNDMORE)
            frame2
         }.forall(_.send(socket, 0))
      }

      def monitor(listener: MonitorEvent => Unit, events: Int*): Socket = {
         val address = s"inproc://monitor-${UUID.randomUUID()}"
         val eventMask = events.filter(e => (e & ZMQ.EVENT_ALL) == e).reduceOption(_|_).getOrElse(ZMQ.EVENT_ALL)

         if (socket.monitor(address, eventMask)) {
            val socketMonitor = context.createSocket(ZMQ.PAIR)
            socketMonitor.connect(address)
            ZThread.start { _ =>
               Breaks.breakable {
                  while (!(Thread.currentThread().isInterrupted || socketMonitor.isClosed)) {
                     Try(Event.recv(socketMonitor)) match {
                        case Success(evt) =>
                           if (evt != null && (evt.getEvent & eventMask) == evt.getEvent) {
                              listener(MonitorEvent(evt.getEvent, Url.parse(evt.getAddress).hostOption.map(_.value).getOrElse("")))
                           }
                        case Failure(_) => Breaks.break
                     }
                  }
               }
            }
            return socketMonitor
         }
         null
      }
   }

   case class MonitorEvent(event: Int, address: String)

}
