package transit.littlecity

import monix.eval.Task
import monix.execution.Scheduler.Implicits.global
import transit.littlecity.config.AppBinding
import transit.littlecity.services.TransitMessagingService
import wvlet.log.{LogFormatter, LogSupport, Logger}

import scala.concurrent.Await

object Main extends App with LogSupport {

   import scala.concurrent.duration._

   AppBinding.startWithSession { session =>
      Logger.setDefaultFormatter(session.build[LogFormatter])

      val transitService = session.build[TransitMessagingService]
      transitService.startPublishing()

      Await.ready(Task.never.runToFuture, 1.day)
   }
}
