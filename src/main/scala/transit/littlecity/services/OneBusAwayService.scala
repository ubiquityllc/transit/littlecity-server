package transit.littlecity.services

import transit.littlecity.data.message.{Agency, RouteArrivalDeparture}
import transit.littlecity.data.model.BusStop

import scala.concurrent.Future

trait OneBusAwayService {
   /**
    * Get information about the agency
    *
    * @return The agency data
    */
   def agencyInfo: Future[Agency]

   /**
    * Finds the stops available for this service
    *
    * @return The list of stops
    */
   def findAvailableStops: Future[List[BusStop]]

   /**
    * Get information about a particular stop
    * @param stopId The id of the stop
    * @return The stop
    */
   def findStopInfo(stopId: String): Future[BusStop]

   /**
    * Finds a list of routes arriving and departing to/from a given stop
    *
    * @param stopId The stop we are interested in
    * @return A list of the routes
    */
   def arrivalsAndDeparturesForStop(stopId: String): Future[List[RouteArrivalDeparture]]
}
