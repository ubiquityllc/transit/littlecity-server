package transit.littlecity.services

trait TransitMessagingService {
   /**
    * Register a new bus stop
    *
    * @param stopId The id of the stop to publish information for
    */
   def registerBusStopNode(stopId: String): Unit

   /**
    * Stops publishing information about this bus stop
    *
    * @param stopId The id of the stop to stop publishing
    */
   def unregisterBusStopNode(stopId: String): Unit

   /**
    * Starts the publisher
    */
   def startPublishing(): Unit

   /**
    * Stop publishing
    */
   def stopPublishing(): Unit
}
