package transit.littlecity.services

import org.zeromq.ZMQ.Socket
import org.zeromq.core.ZAuth

trait AuthenticationService {
   protected def auth: ZAuth

   /**
    * Configures the socket as a server
    * @param socket The socket
    */
   def configureServer(socket: Socket): Unit
}
