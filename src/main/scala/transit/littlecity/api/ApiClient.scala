package transit.littlecity.api

import java.io.File

import gigahorse._
import gigahorse.support.okhttp.Gigahorse
import io.lemonlabs.uri.dsl._
import io.lemonlabs.uri.{RelativeUrl, Url}

import scala.concurrent.{ExecutionContext, Future}

class ApiClient(apiUrl: String,
                apiBasePath: String,
                delegate: HttpClient) extends HttpClient {

   private val apiBaseUrl: Url = apiUrl / apiBasePath

   override def underlying[A]: A = delegate.underlying

   override def close(): Unit = delegate.close()

   override def run(request: Request): Future[FullResponse] = delegate.run(prependBase(request))

   private def prependBase(request: Request): Request = Url.parse(request.url) match {
      case _: RelativeUrl => request.withUrl(apiBaseUrl / request.url)
      case _ => request
   }

   override def run[A](request: Request, f: FullResponse => A): Future[A] = delegate.run(prependBase(request), f)

   override def run[A](request: Request, lifter: FutureLifter[A])(implicit ec: ExecutionContext): Future[Either[Throwable, A]] = delegate.run(prependBase(request), lifter)

   override def download(request: Request, file: File): Future[File] = delegate.download(prependBase(request), file)

   override def processFull(request: Request): Future[FullResponse] = delegate.processFull(prependBase(request))

   override def processFull[A](request: Request, f: FullResponse => A): Future[A] = delegate.processFull(prependBase(request), f)

   override def processFull[A](request: Request, lifter: FutureLifter[A])(implicit ec: ExecutionContext): Future[Either[Throwable, A]] = delegate.processFull(prependBase(request), lifter)

   override def websocket(request: Request)(handler: PartialFunction[WebSocketEvent, Unit]): Future[WebSocket] = delegate.websocket(prependBase(request))(handler)
}

object ApiClient {
   def apply(apiUrl: String, apiBasePath: String = "/"): HttpClient =
      new ApiClient(apiUrl, apiBasePath, Gigahorse.http(Gigahorse.config))
}
